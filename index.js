var bluebird = require('bluebird')

/*
    store is any object that implements set, get and is in an async manner
    this could be used for local storage or database storage
*/
module.exports = function (store) {
    var otp = {}
    otp.create = function (identifier, length) {
        var code = makeid(length)

        return store.set(identifier, code)
            .then(function () {
                return bluebird.resolve(code)
            })

    }

    otp.send = function (identifier, carrier) {
        //TODO - write code to send sms
        return store.get(identifier)
            .then(function (code) {
                console.log('code is ', code)
                return bluebird.resolve()
            })
    }

    otp.verify = function (identifier, code) {
        return store.get(identifier)
            .then(function (stored_code) {
                console.log('stored code is ', stored_code)
                console.log('entered code is ', code)
                if (stored_code == code) {
                    return bluebird.resolve(true)
                }
                return bluebird.resolve(false)
            })
    }

    function makeid(len) {
        var text = "";
        var possible = "0123456789";

        for (var i = 0; i < len; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    return otp
}
